import 'package:flutter/material.dart';
import 'screen/MainScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '观察者',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: MainScreen(title: '主页'),
    );
  }
}
