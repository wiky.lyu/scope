import 'package:flutter/material.dart';
import '../container/NewsContentView.dart';
import '../api/model.dart';
import '../api/news.dart';

/* 新闻详情 */
class NewsScreen extends StatefulWidget {
  final String id;
  final int type;
  final String title;

  NewsScreen(
      {Key key, @required this.id, @required this.title, @required this.type})
      : super(key: key);

  @override
  NewsScreenState createState() => NewsScreenState();
}

class NewsScreenState extends State<NewsScreen> {
  NewsContent _newsContent;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    this._isLoading = true;
    fetchNewsContent(this.widget.id).then((newsContent) {
      this.setState(() {
        this._isLoading = false;
        this._newsContent = newsContent;
      });
    }).catchError((e) {
      print(e);
      this.setState(() {
        this._isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: this._isLoading
            ? Center(child: CircularProgressIndicator())
            : (this._newsContent == null
                ? Center(child: Text('文章获取失败'))
                : (NewsContentView(
                    newsContent: this._newsContent,
                  ))));
  }
}
