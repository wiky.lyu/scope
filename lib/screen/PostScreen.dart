import 'package:flutter/material.dart';
import '../container/NewsContentView.dart';
import '../api/model.dart';
import '../api/news.dart';

class PostScreen extends StatefulWidget {
  final int id;
  final int type;
  final String title;

  PostScreen(
      {Key key, @required this.id, @required this.title, @required this.type})
      : super(key: key);

  @override
  PostScreenState createState() => PostScreenState();
}

class PostScreenState extends State<PostScreen> {
  Post _post;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    this._isLoading = true;
    fetchPost(this.widget.id).then((post) {
      this.setState(() {
        this._isLoading = false;
        this._post = post;
      });
    }).catchError((e) {
      print(e);
      this.setState(() {
        this._isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: this._isLoading
            ? Center(child: CircularProgressIndicator())
            : (this._post == null
                ? Center(child: Text('文章获取失败'))
                : (NewsContentView(
                    newsContent: this._post,
                  ))));
  }
}
