import 'package:flutter/material.dart';
import '../container/NewsList.dart';
import '../container/PostList.dart';
import '../api/model.dart';
import '../api/news.dart';
import '../api/main.dart';

Future<List<News>> fetchYaoWenListWrapper(int pageNo) async {
  return fetchYaoWenNews(pageNo: pageNo);
}

Future<List<Post>> fetchPostsListWrapper(int pageNo) async {
  return fetchPostList(order: 2, pageNo: pageNo);
}

/* 主页 */
class MainScreen extends StatefulWidget {
  MainScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MainScreenState createState() => MainScreenState();
}

class MainScreenState extends State<MainScreen> with TickerProviderStateMixin {
  PageController _pageController;
  int _tab = 0;

  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    this._pageController = PageController(keepPage: true);
  }

  @override
  void dispose() {
    this._pageController.dispose();
    super.dispose();
  }

  onPageChanged(int value) {
    this.setState(() {
      this._tab = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: this._scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: PageView(
        key: PageStorageKey<String>('main-tabbarview'),
        controller: this._pageController,
        onPageChanged: this.onPageChanged,
        children: <Widget>[
          NewsList(fetchYaoWenListWrapper,
              key: PageStorageKey<String>('page.news')),
          PostList(fetchPostsListWrapper,
              key: PageStorageKey<String>('page.community'))
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int value) {
          this._pageController.animateToPage(value,
              duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
          this.setState(() {
            this._tab = value;
          });
        },
        currentIndex: this._tab,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.radio), title: Text('要闻')),
          BottomNavigationBarItem(icon: Icon(Icons.message), title: Text('风闻')),
        ],
      ),
    );
  }
}
