import 'package:flutter/material.dart';
import '../container/NewsList.dart';
import '../api/model.dart';
import '../api/news.dart';

FetchNewsFunc fetchNewsListWrapper(String id, int type) {
  Future<List<News>> f(int p) async {
    return fetchNewsList(id, type, pageNo: p);
  }

  return f;
}

class NewsListScreen extends StatefulWidget {
  final String id;
  final int type;
  final String title;

  NewsListScreen(
      {Key key, @required this.id, @required this.type, @required this.title})
      : super(key: key);
  NewsListScreenState createState() => NewsListScreenState();
}

class NewsListScreenState extends State<NewsListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: NewsList(fetchNewsListWrapper(this.widget.id, this.widget.type)),
    );
  }
}
