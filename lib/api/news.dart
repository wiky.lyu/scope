import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'model.dart';
import 'db.dart';

/* 获取要闻列表 */
Future<List<News>> fetchYaoWenNews({pageNo: 1}) async {
  var url =
      'https://app.guancha.cn/news/yaowen-new.json?pageNo=' + pageNo.toString();
  final response = await http.get(url);
  if (response.statusCode != 200) {
    throw Exception('failed to load yaowen-news');
  }
  var body = json.decode(response.body);
  var yaowenNews = new List<News>();
  if (body['code'] != 0 || body['data'].length == 0) {
    return yaowenNews;
  }
  for (var i = 0; i < body['data']['items'].length; i++) {
    var yaowen = News.fromJson(body['data']['items'][i]);
    if (yaowen != null) {
      yaowenNews.add(yaowen);
    }
  }
  return yaowenNews;
}

/* 获取新闻列表 */
Future<List<News>> fetchNewsList(String id, int type, {pageNo: 1}) async {
  var url = 'https://app.guancha.cn/news/common-list.json?id=' +
      id +
      '&pageNo=' +
      pageNo.toString() +
      '&type=' +
      type.toString();
  print(url);
  final response = await http.get(url);
  if (response.statusCode != 200) {
    throw Exception('failed to load common list');
  }
  var body = json.decode(response.body);
  var newsList = new List<News>();
  if (body['code'] != 0 || body['data'].length == 0) {
    return newsList;
  }
  for (var i = 0; i < body['data']['items'].length; i++) {
    var news = News.fromJson(body['data']['items'][i]);
    if (news != null) {
      newsList.add(news);
    }
  }
  return newsList;
}

Future<NewsContent> fetchNewsContent(String id, {int type = 1}) async {
  var c = await DBHelper().getNewsContent(id);
  if (c != null) {
    return c;
  }
  var url = 'https://app.guancha.cn/news/content?id=' +
      id +
      '&type=' +
      type.toString();
  final response = await http.get(url);
  if (response.statusCode != 200) {
    throw Exception('failed to load news content');
  }
  var body = json.decode(response.body);
  if (body['code'] != 0) {
    return null;
  }
  c = NewsContent.fromJson(body['data']);
  await DBHelper().saveNewsContent(c);
  return c;
}

Future<Post> fetchPost(int id) async {
  var url =
      'https://app.guancha.cn/post/get-post-by-id?post_id=' + id.toString();
  final response = await http.get(url);
  if (response.statusCode != 200) {
    throw Exception('failed to load news content');
  }
  var body = json.decode(response.body);
  if (body['code'] != 0 || body['data'].length == 0) {
    return null;
  }
  var p = Post.fromJson(body['data'][0]);
  return p;
}
