import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'dart:io';
import 'model.dart';
import "dart:convert";

class DBHelper {
  static Database _db;

  Future<Database> get db async {
    if (DBHelper._db != null) {
      return DBHelper._db;
    }
    DBHelper._db = await this.initDB();
    return DBHelper._db;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "scope.db");
    return await openDatabase(path, version: 1, onCreate: this._onCreate);
  }

  _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE news_content(id text primary key,title text,newsTime text,author text,source text,daodu text,content text,commentNum int,collectionNum int,hasCollection int,editor text,showEditor int)");
  }

  Future<void> saveNewsContent(NewsContent c) async {
    if (c == null) {
      return;
    }
    var db = await this.db;
    var author = jsonEncode(c.author);
    var editor = jsonEncode(c.editor);
    await db.rawInsert(
        "INSERT OR IGNORE INTO news_content(id,title,newsTime,author,source,daodu,content,commentNum,collectionNum,hasCollection,editor,showEditor) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",
        [
          c.id,
          c.title,
          c.newsTime,
          author,
          c.source,
          c.daodu,
          c.content,
          c.commentNum,
          c.collectionNum,
          c.hasCollection ? 1 : 0,
          editor,
          c.showEditor ? 1 : 0
        ]);
  }

  Future<NewsContent> getNewsContent(String id) async {
    var db = await this.db;
    List<Map> result =
        await db.rawQuery("SELECT * FROM news_content WHERE id=?", [id]);
    if (result.length == 0) {
      return null;
    }
    var r = result[0];
    var author = jsonDecode(r['author']);
    var c = NewsContent(
        id: r['id'],
        title: r['title'],
        newsTime: r['newsTime'],
        author: (author == null)
            ? []
            : author.map((i) => Author.fromJson(i)).toList().cast<Author>(),
        source: r['source'],
        daodu: r['daodu'],
        content: r['content'],
        commentNum: r['commentNum'],
        collectionNum: r['collectionNum'],
        hasCollection: r['hasCollection'] > 0 ? true : false,
        editor: Editor.fromJson(jsonDecode(r['editor'])),
        showEditor: r['showEditor'] > 0 ? true : false);
    return c;
  }
}
