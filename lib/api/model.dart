class Special {
  final String id;
  final String name;

  Special({this.id, this.name});

  factory Special.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return Special(id: json['id'], name: json['name']);
  }
}

class Author {
  final String id;
  final String name;
  final String pic;
  final String description;
  final bool isMore;

  Author({this.id, this.name, this.pic, this.description, this.isMore});

  factory Author.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return Author(
        id: json['id'],
        name: json['name'],
        pic: json['pic'],
        description: json['description'],
        isMore: json['is_more']);
  }

  Map<String, dynamic> toJson() {
    if (this == null) {
      return {};
    }
    return {
      "id": id,
      "name": name,
      "pic": pic,
      "description": description,
      "is_more": isMore
    };
  }
}

class User {
  final String id;
  final String nick;
  final String pic;
  final String levelIcon;

  User({this.id, this.nick, this.pic, this.levelIcon});

  factory User.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return User(
        id: json['id'],
        nick: json['nick'],
        pic: json['pic'],
        levelIcon: json['level_icon']);
  }
}

class News {
  final String id;
  final String title;
  final String pic;
  final int showType;
  final String newsTime;
  final int newsType;
  final int commentNum;
  final String url;
  final Special special;
  final Author author;
  final User user;

  News(
      {this.id,
      this.title,
      this.pic,
      this.showType,
      this.newsTime,
      this.newsType,
      this.commentNum,
      this.url,
      this.special,
      this.author,
      this.user});

  factory News.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return News(
        id: json['id'],
        title: json['title'],
        pic: json['pic'],
        showType: json['show_type'],
        newsTime: json['news_time'],
        newsType: json['news_type'],
        commentNum: json['comment_num'],
        url: json['url'],
        special: Special.fromJson(json['special']),
        author: Author.fromJson(json['author']),
        user: User.fromJson(json['user']));
  }
}

class Editor {
  final String name;
  final String pic;
  final String description;

  Editor({this.name, this.pic, this.description});

  factory Editor.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return Editor(
        name: json['name'], pic: json['pic'], description: json['description']);
  }

  Map<String, dynamic> toJson() {
    if (this == null) {
      return {};
    }
    return {"name": name, "pic": pic, "description": description};
  }
}

class NewsContentInterface {
  String getContent() => '';
  String getTitle() => '';
}

class NewsContent implements NewsContentInterface {
  final String id;
  final String title;
  final String newsTime;
  final List<Author> author;
  final String source;
  final String daodu;
  final String content;
  final int commentNum;
  final int collectionNum;
  final bool hasCollection;
  final Editor editor;
  final bool showEditor;

  String getContent() => this.content;
  String getTitle() => this.title;

  NewsContent(
      {this.id,
      this.title,
      this.newsTime,
      this.author,
      this.source,
      this.daodu,
      this.content,
      this.commentNum,
      this.collectionNum,
      this.hasCollection,
      this.editor,
      this.showEditor});

  factory NewsContent.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return NewsContent(
        id: json['id'],
        title: json['title'],
        newsTime: json['news_time'],
        author: (json['author'] == null || json['author'].length == 0)
            ? []
            : json['author']
                .map((i) => Author.fromJson(i))
                .toList()
                .cast<Author>(),
        source: json['source'],
        daodu: json['daodu'],
        content: json['content'],
        commentNum: json['comment_num'],
        collectionNum: json['collection_num'],
        hasCollection: json['has_collection'],
        editor: Editor.fromJson(json['editr']),
        showEditor: json['show_editor']);
  }
}

class Topic {
  final int id;
  final String name;

  Topic({this.id, this.name});

  factory Topic.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    if (json['topic_id'] != null) {
      return Topic(id: json['topic_id'], name: json['topic_name']);
    }
    return Topic(id: json['id'], name: json['name']);
  }
}

class Post implements NewsContentInterface {
  final int id;
  final String title;
  final String summary;
  final String pic;
  final int userId;
  final bool isAnonymous;
  final int commentCount;
  final String createdAt;
  final String passAt;
  final int status;
  final int accessDevice;
  final int viewCount;
  final int orderbyValue;
  final int praiseNum;
  final String userNick;
  final String userPhoto;
  final String userDescription;
  final String userLevel;
  final String content;
  final String contentOrg;
  final String postUrl;
  final String postUrlNight;
  final String shareUrl;
  final int codeType;
  final int collectionCount;
  final bool hasPraise;
  final bool hasCollection;
  final bool hasVote;
  final String userLevelLogo;
  final List<Topic> topics;
  final bool hasAttention;

  String getContent() => this.content;
  String getTitle() => this.title;

  Post(
      {this.id,
      this.title,
      this.summary,
      this.pic,
      this.userId,
      this.isAnonymous,
      this.commentCount,
      this.createdAt,
      this.passAt,
      this.status,
      this.accessDevice,
      this.viewCount,
      this.orderbyValue,
      this.praiseNum,
      this.userNick,
      this.userPhoto,
      this.userDescription,
      this.userLevel,
      this.content,
      this.contentOrg,
      this.postUrl,
      this.postUrlNight,
      this.shareUrl,
      this.codeType,
      this.collectionCount,
      this.hasPraise,
      this.hasCollection,
      this.hasVote,
      this.userLevelLogo,
      this.topics,
      this.hasAttention});

  factory Post.fromJson(Map<String, dynamic> json) {
    if (json == null || json.length == 0) {
      return null;
    }
    return Post(
        id: json['id'],
        title: json['title'],
        summary: json['summary'],
        pic: json['pic'],
        userId: json['user_id'],
        isAnonymous: json['is_anonymous'],
        commentCount: json['comment_count'],
        createdAt: json['created_at'],
        passAt: json['pass_at'],
        status: json['status'],
        accessDevice: json['access_device'],
        viewCount: json['view_count'],
        orderbyValue: json['orderby_value'],
        praiseNum: json['praise_num'],
        userNick: json['user_nick'],
        userPhoto: json['user_photo'],
        userDescription: json['user_description'],
        userLevel: json['user_level'],
        content: json['content'],
        contentOrg: json['content_org'],
        postUrl: json['post_url'],
        postUrlNight: json['post_url_night'],
        shareUrl: json['share_url'],
        codeType: json['code_type'],
        collectionCount: json['collection_count'] is String
            ? int.parse(json['collection_count'])
            : json['collection_count'],
        hasPraise: json['has_praise'],
        hasCollection: json['has_collection'],
        hasVote: json['has_vote'],
        userLevelLogo: json['user_level_logo'],
        topics: json['topics'] == null
            ? []
            : json['topics']
                .map((i) => Topic.fromJson(i))
                .toList()
                .cast<Topic>(),
        hasAttention: json['has_attention']);
  }
}
