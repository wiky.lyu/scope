import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'model.dart';

Future<List<Post>> fetchPostList({pageNo: int, order = 1}) async {
  var url = 'https://app.guancha.cn/main/get-index-list?order=' +
      order.toString() +
      '&page_no=' +
      pageNo.toString();
  final response = await http.get(url);
  if (response.statusCode != 200) {
    throw Exception('failed to load news content');
  }
  var body = json.decode(response.body);
  if (body['code'] != 0 || body['data'].length == 0) {
    return null;
  }
  var posts = new List<Post>();
  for (var i = 0; i < body['data']['items'].length; i++) {
    var p = Post.fromJson(body['data']['items'][i]);
    posts.add(p);
  }
  return posts;
}
