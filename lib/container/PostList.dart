import 'package:flutter/material.dart';
import 'dart:async';
import '../api/model.dart';
import '../widget/PostItem.dart';

typedef Future<List<Post>> FetchPostsFunc(int pageNo);

class PostList extends StatefulWidget {
  final FetchPostsFunc _fetchPostsFunc;
  PostList(this._fetchPostsFunc, {Key key}) : super(key: key);
  PostListState createState() => PostListState();
}

class PostListState extends State<PostList>
    with AutomaticKeepAliveClientMixin<PostList> {
  List<Post> _posts = new List<Post>();
  int _pageNo = 1;
  bool _isLoading = false;
  bool _isRefreshing = false;
  bool _hasMore = true;

  final ScrollController _scrollController = ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  bool get wantKeepAlive => true;

  @override
  initState() {
    super.initState();
    this._scrollController.addListener(this._scrollListener);
    this._refresh();
  }

  _loadMore() {
    if (this._isLoading || this._isRefreshing || !this._hasMore) {
      return;
    }
    this.setState(() {
      this._isLoading = true;
    });
    var pageNo = this._pageNo;

    this.widget._fetchPostsFunc(pageNo).then((posts) {
      if (this._pageNo != pageNo || !this._isLoading || this._isRefreshing) {
        return;
      }
      if (posts.length == 0) {
        this.setState(() {
          this._hasMore = false;
          this._isLoading = false;
        });
        return;
      }
      this.setState(() {
        for (var i = 0; i < posts.length; i++) {
          var isDup = false;
          for (var j = this._posts.length - 5; j < this._posts.length; j++) {
            if (this._posts[j].id == posts[i].id) {
              isDup = true;
              break;
            }
          }
          if (isDup) {
            print('duplicate!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            continue;
          }
          this._posts.add(posts[i]);
        }
        this._pageNo++;
        this._isLoading = false;
      });
    }).catchError(this._onError);
  }

  _onError(e) {
    print(e);
    final snackBar = SnackBar(
      content: Text('网络错误...请检查网络后重试。'),
      duration: Duration(seconds: 3),
    );
    Scaffold.of(context).showSnackBar(snackBar);
    this.setState(() {
      this._isLoading = false;
      this._isRefreshing = false;
    });
  }

  _scrollListener() {
    if (this._scrollController.offset >=
            this._scrollController.position.maxScrollExtent - 220 &&
        !this._isLoading) {
      this._loadMore();
    }
  }

  Future<void> _refresh() {
    this._isRefreshing = true;
    final Completer<void> completer = Completer<void>();

    this.widget._fetchPostsFunc(1).then((posts) {
      this.setState(() {
        this._pageNo = 2;
        this._isLoading = false;
        this._isRefreshing = false;
        this._hasMore = true;
        this._posts = posts;
      });

      completer.complete();
    }).catchError((e) {
      this._onError(e);
      completer.complete();
    });

    return completer.future.then<void>((_) {});
  }

  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
        key: this._refreshIndicatorKey,
        onRefresh: this._refresh,
        child: ListView.builder(
          padding: const EdgeInsets.all(8.0),
          itemCount: this._posts.length + (this._isLoading ? 1 : 0),
          controller: this._scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            if (i >= this._posts.length) {
              return Center(
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: CircularProgressIndicator()));
            }
            return PostItem(post: this._posts[i]);
          },
        ));
  }
}
