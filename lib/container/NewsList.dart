import 'package:flutter/material.dart';
import 'dart:async';
import '../api/model.dart';
import '../widget/NewsItem.dart';

typedef Future<List<News>> FetchNewsFunc(int pageNo);

class NewsList extends StatefulWidget {
  final FetchNewsFunc _fetchNewsFunc;

  NewsList(this._fetchNewsFunc, {PageStorageKey key}) : super(key: key);

  @override
  NewsListState createState() => NewsListState();
}

class NewsListState extends State<NewsList>
    with AutomaticKeepAliveClientMixin<NewsList> {
  List<News> _news = new List<News>();
  int _pageNo = 1;
  bool _isLoading = false;
  bool _isRefreshing = false;
  bool _hasMore = true;

  final ScrollController _scrollController = ScrollController();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    this._scrollController.addListener(this._scrollListener);
    this._refresh();
  }

  _loadMore() {
    if (this._isLoading || this._isRefreshing || !this._hasMore) {
      return;
    }
    this.setState(() {
      this._isLoading = true;
    });
    var pageNo = this._pageNo;
    print('loadMore:' + pageNo.toString());

    this.widget._fetchNewsFunc(pageNo).then((news) {
      if (this._pageNo != pageNo || !this._isLoading || this._isRefreshing) {
        return;
      }
      if (news.length == 0) {
        this.setState(() {
          this._hasMore = false;
          this._isLoading = false;
        });
        return;
      }
      this.setState(() {
        for (var i = 0; i < news.length; i++) {
          var isDup = false;
          for (var j = this._news.length - 5; j < this._news.length; j++) {
            if (this._news[j].id == news[i].id) {
              isDup = true;
              break;
            }
          }
          if (isDup) {
            continue;
          }
          this._news.add(news[i]);
        }
        this._pageNo++;
        this._isLoading = false;
      });
    }).catchError(this._onError);
  }

  _onError(e) {
    print('onError:' + e.toString());
    final snackBar = SnackBar(
      content: Text('网络错误...请检查网络后重试。'),
      duration: Duration(seconds: 3),
    );
    Scaffold.of(context).showSnackBar(snackBar);
    this.setState(() {
      this._isLoading = false;
      this._isRefreshing = false;
      this._hasMore = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: ListView.builder(
          key: PageStorageKey("news-list"),
          padding: const EdgeInsets.all(8.0),
          itemCount: this._news.length + (this._isLoading ? 1 : 0),
          controller: this._scrollController,
          physics: const AlwaysScrollableScrollPhysics(),
          itemBuilder: (context, i) {
            if (i >= this._news.length) {
              return Center(
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: CircularProgressIndicator()));
            }
            return NewsItem(yaowen: this._news[i]);
          },
        ));
  }

  _scrollListener() {
    if (this._scrollController.offset >=
            this._scrollController.position.maxScrollExtent - 220 &&
        !this._isLoading) {
      this._loadMore();
    }
  }

  Future<void> _refresh() {
    this._isRefreshing = true;
    final Completer<void> completer = Completer<void>();

    this.widget._fetchNewsFunc(1).then((news) {
      this.setState(() {
        this._pageNo = 2;
        this._isLoading = false;
        this._isRefreshing = false;
        this._hasMore = true;
        this._news = news;
      });

      completer.complete();
    }).catchError((e) {
      this._onError(e);
      completer.complete();
    });

    return completer.future.then<void>((_) {});
  }

  @override
  void setState(fn) {
    if (!this.mounted) {
      return;
    }
    super.setState(fn);
  }
}
