import 'package:flutter/material.dart';
import '../api/model.dart';
import 'package:flutter_html/flutter_html.dart';

class NewsContentView extends StatefulWidget {
  final NewsContentInterface newsContent;

  NewsContentView({Key key, @required this.newsContent}) : super(key: key);

  NewsContentViewState createState() => NewsContentViewState();
}

class NewsContentViewState extends State<NewsContentView> {
  @override
  Widget build(BuildContext context) {
    var newsContent = this.widget.newsContent;
    return Container(
        padding: EdgeInsets.only(left: 12, right: 12),
        child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    newsContent.getTitle(),
                    style: TextStyle(fontSize: 24),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Html(
                    data: newsContent.getContent(),
                  ),
                )
              ],
            )));
  }
}
