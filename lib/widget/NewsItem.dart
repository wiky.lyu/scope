import 'package:flutter/material.dart';
import '../api/model.dart';
import '../screen/NewsScreen.dart';
import '../screen/NewsListScreen.dart';
import '../screen/PostScreen.dart';
import 'UserAvatar.dart';

class NewsItem extends StatefulWidget {
  final News yaowen;
  NewsItem({Key key, @required this.yaowen}) : super(key: key);

  @override
  NewsItemState createState() => new NewsItemState();
}

class NewsItemState extends State<NewsItem> with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    this.controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    this.animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    this.controller.forward();
  }

  @override
  void deactivate() {
    super.deactivate();
    this.controller.stop();
  }

  @override
  Widget build(BuildContext context) {
    final yaowen = this.widget.yaowen;
    Widget child;

    // print(yaowen.title +
    //     '\tid=' +
    //     yaowen.id +
    //     '\tshowType=' +
    //     yaowen.showType.toString() +
    //     '\tnewsType=' +
    //     yaowen.newsType.toString());
    /* 根据showtype不同显示 不同的样式 */
    if (yaowen.showType == 1) {
      child = this._buildStyle1(yaowen);
    } else if (yaowen.showType == 2) {
      child = this._buildStyle2(yaowen);
    } else if (yaowen.showType == 3) {
      child = this._buildStyle3(yaowen);
    } else if (yaowen.showType == 4) {
      child = this._buildStyle4(yaowen);
    } else if (yaowen.showType == 5) {
      child = this._buildStyle5(yaowen);
    } else if (yaowen.showType == 6) {
      child = this._buildStyle6(yaowen);
    } else if (yaowen.showType == 7) {
      child = this._buildStyle7(yaowen);
    } else {
      child = Text('未知结构类型，请联系开发者 show_type=' + yaowen.showType.toString());
    }

    return FadeTransition(
        opacity: this.animation,
        child: Card(
            child: Material(
          child: InkWell(
            onTap: this._onItemPressed,
            child: Padding(child: child, padding: EdgeInsets.all(6)),
          ),
        )));
  }

  _onItemPressed() {
    var yaowen = this.widget.yaowen;
    if (yaowen.newsType == 1) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NewsScreen(
                    id: yaowen.id,
                    title: yaowen.title,
                    type: yaowen.newsType,
                  )));
    } else if (yaowen.newsType == 2) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PostScreen(
                    id: int.parse(yaowen.id),
                    title: yaowen.title,
                    type: yaowen.newsType,
                  )));
    } else if (yaowen.newsType == 5) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => NewsListScreen(
                  id: yaowen.id, type: 4, title: '专题：' + yaowen.title)));
    }
  }

  Widget _buildTitle(title, {maxLines: 2}) {
    return Padding(
        padding: EdgeInsets.only(left: 4, right: 4, top: 2, bottom: 2),
        child: Text(
          title,
          style: TextStyle(fontSize: 16),
          maxLines: maxLines,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
        ));
  }

  Widget _buildBottomInfo(special, author, newsTime, commentNum) {
    var children = <Widget>[];
    if (author != null) {
      children.add(Text(
        author.name,
        style: TextStyle(fontSize: 12, color: Colors.black54),
      ));
      children.add(Spacer());
    }
    if (special != null) {
      children.add(Text(
        special.name,
        style: TextStyle(fontSize: 12, color: Colors.black54),
      ));
      if (author == null) {
        children.add(Spacer());
      }
    }
    children.add(Container(
        margin: EdgeInsets.only(left: 8),
        child: Text(
          newsTime,
          style: TextStyle(fontSize: 12, color: Colors.black54),
        )));
    children.add(Container(
        margin: EdgeInsets.only(left: 8),
        child: Row(
          children: <Widget>[
            Icon(Icons.message, size: 14),
            Container(
                margin: EdgeInsets.only(left: 2),
                child: Text(
                  commentNum.toString(),
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                )),
          ],
        )));
    return Container(
        padding: EdgeInsets.only(left: 4, right: 4, top: 2, bottom: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: children,
        ));
  }

  Widget _buildStyle1(News yaowen) {
    return Container(
        height: 88,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network(yaowen.pic, width: 125, fit: BoxFit.contain),
            Expanded(
              child: Padding(
                  padding: EdgeInsets.only(left: 6, right: 6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      this._buildTitle(yaowen.title),
                      this._buildBottomInfo(yaowen.special, null,
                          yaowen.newsTime, yaowen.commentNum)
                    ],
                  )),
            ),
          ],
        ));
  }

  Widget _buildStyle2(News yaowen) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          this._buildTitle(yaowen.title, maxLines: 1),
          Container(
            margin: EdgeInsets.only(top: 2, bottom: 2),
            child: Image.network(yaowen.pic, height: 145, fit: BoxFit.cover),
          ),
          this._buildBottomInfo(
              yaowen.special, null, yaowen.newsTime, yaowen.commentNum)
        ],
      ),
    );
  }

  Widget _buildStyle3(News yaowen) {
    var pics = yaowen.pic.split(',');
    var images = <Widget>[];
    for (var i = 0; i < pics.length; i++) {
      images.add(Expanded(
          flex: 1,
          child: Container(
            height: 88,
            child: Image.network(pics[i], fit: BoxFit.cover),
          )));
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          this._buildTitle(yaowen.title, maxLines: 1),
          Row(
            children: images,
          ),
          this._buildBottomInfo(
              yaowen.special, null, yaowen.newsTime, yaowen.commentNum)
        ],
      ),
    );
  }

  Widget _buildStyle4(News yaowen) {
    return Container(
        height: 80,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Image.network(
              yaowen.pic,
              width: 60,
              fit: BoxFit.cover,
            ),
            Flexible(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      this._buildTitle(yaowen.title),
                      this._buildBottomInfo(yaowen.special, yaowen.author,
                          yaowen.newsTime, yaowen.commentNum)
                    ],
                  ),
                ))
          ],
        ));
  }

  Widget _buildStyle5(News yaowen) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        this._buildTitle(yaowen.title),
        this._buildBottomInfo(
            yaowen.special, null, yaowen.newsTime, yaowen.commentNum)
      ],
    ));
  }

  Widget _buildStyle6(News yaowen) {
    return Container(
        child: Image.network(
      yaowen.pic,
      height: 70,
      fit: BoxFit.cover,
    ));
  }

  Widget _buildStyle7(News yaowen) {
    return Container(
      child: Column(
        children: <Widget>[
          UserAvatar(
            userAvatar: yaowen.user.pic,
            userName: yaowen.user.nick,
          ),
          this._buildStyle1(yaowen)
        ],
      ),
    );
  }
}
