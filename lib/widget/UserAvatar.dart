import 'package:flutter/material.dart';
import '../api/model.dart';

class UserAvatar extends StatelessWidget {
  final String userAvatar;
  final String userName;

  const UserAvatar(
      {Key key, @required this.userName, @required this.userAvatar})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      child: Row(
        children: <Widget>[
          Container(
            width: 26,
            height: 26,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.fill, image: NetworkImage(this.userAvatar))),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: Text(this.userName,
                style: TextStyle(fontSize: 14, color: Colors.black54)),
          )
        ],
      ),
    );
  }
}
