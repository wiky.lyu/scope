import 'package:flutter/material.dart';
import '../api/model.dart';
import '../screen/NewsScreen.dart';
import '../screen/NewsListScreen.dart';
import '../screen/PostScreen.dart';
import 'UserAvatar.dart';

class PostItem extends StatefulWidget {
  final Post post;
  PostItem({Key key, @required this.post}) : super(key: key);

  @override
  PostItemState createState() => new PostItemState();
}

class PostItemState extends State<PostItem> with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    this.controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    this.animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    this.controller.forward();
  }

  @override
  void deactivate() {
    super.deactivate();
    this.controller.stop();
  }

  @override
  Widget build(BuildContext context) {
    final post = this.widget.post;
    Widget child;

    /* 根据showtype不同显示 不同的样式 */
    child = this._buildStyle(post);

    return FadeTransition(
        opacity: this.animation,
        child: Card(
            child: Material(
          child: InkWell(
            onTap: this._onItemPressed,
            child: Padding(child: child, padding: EdgeInsets.all(6)),
          ),
        )));
  }

  _onItemPressed() {
    var post = this.widget.post;
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PostScreen(
                  id: post.id,
                  title: post.title,
                  type: 1,
                )));
  }

  Widget _buildTitle(title, {maxLines: 2}) {
    return Padding(
        padding: EdgeInsets.only(left: 4, right: 4, top: 2, bottom: 2),
        child: Text(
          title,
          style: TextStyle(fontSize: 16),
          maxLines: maxLines,
          softWrap: true,
          overflow: TextOverflow.ellipsis,
        ));
  }

  Widget _buildBottomInfo(Post post) {
    var children = <Widget>[];

    var topicName = '';
    for (var i = 0; i < post.topics.length; i++) {
      topicName = post.topics[i].name;
      break;
    }

    children.add(Text(
      topicName,
      style: TextStyle(fontSize: 12, color: Colors.black54),
    ));
    children.add(Spacer());

    children.add(Container(
        margin: EdgeInsets.only(left: 8),
        child: Text(
          post.createdAt,
          style: TextStyle(fontSize: 12, color: Colors.black54),
        )));
    children.add(Container(
        margin: EdgeInsets.only(left: 8),
        child: Row(
          children: <Widget>[
            Icon(Icons.message, size: 14),
            Container(
                margin: EdgeInsets.only(left: 2),
                child: Text(
                  post.commentCount.toString(),
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                )),
          ],
        )));
    return Container(
        padding: EdgeInsets.only(left: 4, right: 4, top: 2, bottom: 2),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: children,
        ));
  }

  Widget _buildStyle2(Post post) {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          this._buildTitle(post.title),
          this._buildBottomInfo(post)
        ],
      ),
    );
  }

  Widget _buildStyle1(Post post) {
    return Container(
        height: 88,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network(post.pic, width: 125, fit: BoxFit.contain),
            Expanded(
              child: Padding(
                  padding: EdgeInsets.only(left: 6, right: 6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      this._buildTitle(post.title),
                      this._buildBottomInfo(post)
                    ],
                  )),
            ),
          ],
        ));
  }

  Widget _buildStyle(Post post) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          UserAvatar(
            userName: post.userNick,
            userAvatar: post.userPhoto,
          ),
          post.pic != '' ? this._buildStyle1(post) : this._buildStyle2(post)
        ],
      ),
    );
  }
}
